package com.example.becki.wgutracker;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class AddTerm extends AppCompatActivity {

    Context context = this;
    EditText editText;
    EditText editDate;
    EditText editDate2;
    Calendar myCalendar = Calendar.getInstance();
    Calendar myCalendar2 = Calendar.getInstance();
    String dateFormat = "MM/dd/yyyy";
    DatePickerDialog.OnDateSetListener date;
    DatePickerDialog.OnDateSetListener date2;
    SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.getDefault());
    DBOpenHelper mDBOpenHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_term);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        editText = (EditText) findViewById(R.id.editText);
        editDate = (EditText) findViewById(R.id.editText2);
        editDate2 = (EditText) findViewById(R.id.editText3);
        mDBOpenHelper = new DBOpenHelper(getApplicationContext());

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String newEntry = editText.getText().toString();
                String newEntryStart = editDate.getText().toString();
                String newEntryEnd = editDate2.getText().toString();
                AddTerm(newEntry, newEntryStart, newEntryEnd);
                editText.setText("");

                startActivity(new Intent(AddTerm.this, ListOfTerms.class));
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        calendarPicker(R.id.editText2);
        calendarPicker2(R.id.editText3);
    }

    public void AddTerm(String newEntry, String newEntryStart, String newEntryEnd) {
        boolean insertData = mDBOpenHelper.addTermData(newEntry, newEntryStart, newEntryEnd);
        if (insertData) {
            toastMessage("Data Successfully Inserted");
        } else {
            toastMessage("Something went wrong");
        }
    }

    public void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

        private void calendarPicker(int textboxId){
        editDate = (EditText) findViewById(textboxId);
        long currentdate = System.currentTimeMillis();
        String dateString = sdf.format(currentdate);
        editDate.setText(dateString);

        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDate();
            }

        };

        editDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(context, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        }

    private void calendarPicker2(int textboxId){
        editDate2 = (EditText) findViewById(textboxId);
        long currentdate = System.currentTimeMillis();
        String dateString = sdf.format(currentdate);
        editDate2.setText(dateString);

        date2 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar2.set(Calendar.YEAR, year);
                myCalendar2.set(Calendar.MONTH, monthOfYear);
                myCalendar2.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDate2();
            }

        };

        editDate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(context, date2, myCalendar2.get(Calendar.YEAR), myCalendar2.get(Calendar.MONTH), myCalendar2.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void updateDate() {
        editDate.setText(sdf.format(myCalendar.getTime()));
    }
    private void updateDate2() {
        editDate2.setText(sdf.format(myCalendar2.getTime()));
    }
}


