package com.example.becki.wgutracker;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MentorRecyclerViewAdapter extends
  RecyclerView.Adapter<MentorRecyclerViewAdapter.ViewHolder> {

    private List<Mentor> mentors;
    private String TAG = "MentorRecyclerViewAdapter";

    public MentorRecyclerViewAdapter(List<Mentor> mentors) {
        this.mentors = mentors;

        Log.d(TAG, "recycler contains mentors:");
        for (Mentor m : mentors){
            Log.d(TAG, "Name: " + m.getName());
        }
    }

    @Override
    public MentorRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.mentor_row, null);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MentorRecyclerViewAdapter.ViewHolder viewHolder, int i) {
        final int pos = i;

        viewHolder.textName.setText(mentors.get(pos).getName());
        viewHolder.textEmail.setText(mentors.get(pos).getEmail());
        viewHolder.textPhone.setText(mentors.get(pos).getPhone());
        viewHolder.chkSelected.setChecked(mentors.get(pos).isSelected());

        viewHolder.chkSelected.setTag(mentors.get(pos));

        viewHolder.chkSelected.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox check = (CheckBox) v;
                Mentor activeMentor = (Mentor) check.getTag();

                activeMentor.setSelected(check.isChecked());
                mentors.get(pos).setSelected(check.isChecked());

                Toast.makeText(
                        v.getContext(),
                        "Clicked on Checkbox: " + check.getText() + " is " + check.isChecked(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mentors.size();

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textName;
        public TextView textEmail;
        public TextView textPhone;
        public CheckBox chkSelected;

        public ViewHolder(View v) {
            super(v);

            textName = (TextView) v.findViewById(R.id.textName);
            textEmail = (TextView) v.findViewById(R.id.textEmail);
            textPhone = (TextView) v.findViewById(R.id.textPhone);
            chkSelected = (CheckBox) v.findViewById(R.id.chkSelected);

        }

    }
}
