package com.example.becki.wgutracker;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class AddMentorFromEditCourse extends AppCompatActivity {

    EditText editName;
    EditText editPhone;
    EditText editEmail;
    DBOpenHelper mDBOpenHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_mentor_from_edit_course);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        editName = (EditText) findViewById(R.id.editTitle);
        editPhone = (EditText) findViewById(R.id.editText5);
        editEmail = (EditText) findViewById(R.id.editText11);
        mDBOpenHelper = new DBOpenHelper(getApplicationContext());

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();

                String newMentorName = editName.getText().toString();
                String newMentorPhone = editPhone.getText().toString();
                String newMentorEmail = editEmail.getText().toString();
                AddMentor(newMentorName, newMentorPhone, newMentorEmail);
                startActivity(new Intent(AddMentorFromEditCourse.this, ListOfMentorsFromEdit.class));
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void AddMentor(String name, String phone, String email) {
        boolean insertData = mDBOpenHelper.addMentorData(name, phone, email);
        if (insertData) {
            toastMessage("Data Successfully Inserted");
        } else {
            toastMessage("Something went wrong");
        }
    }

    public void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

}
