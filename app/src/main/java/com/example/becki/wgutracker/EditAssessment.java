package com.example.becki.wgutracker;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class EditAssessment extends AppCompatActivity {

    Context context = this;
    String TAG = "EditAssessment";
    EditText editDate;
    EditText editDate2;
    Calendar myCalendar = Calendar.getInstance();
    Calendar myCalendar2 = Calendar.getInstance();
    String dateFormat = "MM/dd/yyyy";
    DatePickerDialog.OnDateSetListener date;
    DatePickerDialog.OnDateSetListener date2;
    SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.getDefault());

    EditText editTitle;
    Spinner spinner;
    EditText editDueDate;
    EditText editGoalDate;
    CheckBox checkBox;
    RadioButton perfButton;
    RadioButton objButton;

    DBOpenHelper mDBOpenHelper;

    private String selectedTitle;
    private int selectedId;
    private String selectedGoal;
    private String selectedDue;
    private boolean selectedAlert;
    private boolean selectedPerf;
    private boolean selectedObj;
    private int selectedCourseId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_assessment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        editTitle = (EditText) findViewById(R.id.editTitle);
        spinner = (Spinner) findViewById(R.id.spinner);
        editDueDate = (EditText) findViewById(R.id.editDueDate);
        editGoalDate = (EditText) findViewById(R.id.editGoalDate);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        perfButton = (RadioButton) findViewById(R.id.perfButton);
        objButton = (RadioButton) findViewById(R.id.objButton);

        mDBOpenHelper = new DBOpenHelper(getApplicationContext());

        Intent receivedIntent = getIntent();

        selectedId = receivedIntent.getIntExtra("id", -1);
        selectedTitle = receivedIntent.getStringExtra("title");
        selectedGoal = receivedIntent.getStringExtra("goal");
        selectedDue = receivedIntent.getStringExtra("due");
        selectedAlert = receivedIntent.getBooleanExtra("alert", false);
        selectedPerf = receivedIntent.getBooleanExtra("isPerf", false);
        selectedObj = receivedIntent.getBooleanExtra("isObj", false);
        selectedCourseId = receivedIntent.getIntExtra("courseId", -1);

        editTitle.setText(selectedTitle);
        editGoalDate.setText(selectedGoal);
        editDueDate.setText(selectedDue);
        checkBox.setChecked(selectedAlert);
        perfButton.setChecked(selectedPerf);
        objButton.setChecked(selectedObj);

        Cursor data = mDBOpenHelper.getCourseData();
        ArrayList<String> listData = new ArrayList<>();
        final ArrayList<Integer> courseIds = new ArrayList<>();

        while(data.moveToNext()) {
            listData.add(data.getString(4));
            courseIds.add(data.getInt(0));
        }

        SpinnerAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listData);
        spinner.setAdapter(adapter);

        spinner.setSelection(courseIds.indexOf(selectedCourseId));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String assessmentTitle = editTitle.getText().toString();
                String goal = editGoalDate.getText().toString();
                String due = editDueDate.getText().toString();
                boolean alert = checkBox.isChecked();
                boolean performance = perfButton.isChecked();
                //boolean objective = !perfButton.isChecked();
                int courseId = courseIds.get(spinner.getSelectedItemPosition());

                mDBOpenHelper.updateAssessment(assessmentTitle, selectedId, selectedTitle, goal, due, alert, performance, courseId);

                SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
                Date dt = new Date();
                try {
                    dt = formatter.parse(goal);
                } catch (ParseException e) {

                }
                Calendar cal = Calendar.getInstance();
                cal.setTime(dt);
                cal.add(Calendar.DATE, -1);

                long alertDateMillis = cal.getTimeInMillis();
                Log.d(TAG, "alertDateMillis: " + alertDateMillis);

                if (alert) {
                    //Log.d(TAG, "Went into if(alert)");
                    Intent intent = new Intent(getApplicationContext(), MyReceiver.class);
                    intent.putExtra("title", "Assessment reminder!");
                    intent.putExtra("text", "Assessment " + editTitle.getText() + " is in one day.");

                    PendingIntent sender = PendingIntent.getBroadcast(EditAssessment.this, selectedId, intent, 0);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, alertDateMillis, sender);
                } else{
                    Intent intent = new Intent(getApplicationContext(), MyReceiver.class);
                    PendingIntent sender = PendingIntent.getBroadcast(EditAssessment.this, selectedId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    alarmManager.cancel(sender);
                }

                startActivity(new Intent(EditAssessment.this, ListOfAssessments.class));

            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        calendarPicker(R.id.editDueDate);
        calendarPicker2(R.id.editGoalDate);
    }

    public void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void calendarPicker(int textboxId){
        editDate = (EditText) findViewById(textboxId);
        //long currentdate = System.currentTimeMillis();
        //String dateString = sdf.format(currentdate);
        //editDate.setText(dateString);

        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDate();
            }

        };

        editDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(context, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    private void calendarPicker2(int textboxId){
        editDate2 = (EditText) findViewById(textboxId);
        //long currentdate = System.currentTimeMillis();
        //String dateString = sdf.format(currentdate);
        //editDate2.setText(dateString);

        date2 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar2.set(Calendar.YEAR, year);
                myCalendar2.set(Calendar.MONTH, monthOfYear);
                myCalendar2.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDate2();
            }

        };

        editDate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(context, date2, myCalendar2.get(Calendar.YEAR), myCalendar2.get(Calendar.MONTH), myCalendar2.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    private void updateDate() {
        editDate.setText(sdf.format(myCalendar.getTime()));
    }

    private void updateDate2() {
        editDate2.setText(sdf.format(myCalendar2.getTime()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_edit_screens, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                //mDBOpenHelper.deleteMentor(selectedId);
                Log.d(TAG, "Clicked Back Arrow Successfully");
                return true;

            case R.id.action_delete:
                Log.d(TAG, "Clicked Trash Successfully");
                mDBOpenHelper.deleteAssessment(selectedId);
                startActivity(new Intent(EditAssessment.this, ListOfAssessments.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
