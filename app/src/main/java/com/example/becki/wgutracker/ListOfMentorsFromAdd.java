package com.example.becki.wgutracker;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListOfMentorsFromAdd extends AppCompatActivity {

    private ListView mListView;
    DBOpenHelper mDBOpenHelper;
    String TAG = "ListOfMentorsFromAdd";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_mentors_from_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mListView = (ListView) findViewById(android.R.id.list);
        mDBOpenHelper = new DBOpenHelper(this);
        populateListView();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                        startActivity(new Intent(ListOfMentorsFromAdd.this, AddMentor.class));
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void populateListView() {
        Cursor data = mDBOpenHelper.getMentorData();
        ArrayList<String> nameList = new ArrayList<>();
        final ArrayList<String> phoneList = new ArrayList<>();
        final ArrayList<String> emailList = new ArrayList<>();

        while(data.moveToNext()) {
            nameList.add(data.getString(1));
            phoneList.add(data.getString(2));
            emailList.add(data.getString(3));
        }

        ListAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, nameList);
        mListView.setAdapter(adapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String name = adapterView.getItemAtPosition(i).toString();
                Log.d(TAG, "onItemClick: You clicked on " + name);

                Cursor data = mDBOpenHelper.getMentorId(name);

                int nameId = -1;
                while (data.moveToNext()){
                    nameId = data.getInt(0);
                }
                if(nameId > -1) {
                    Log.d(TAG, "The id is: " + nameId);
                    //Log.d(TAG, "The start is: " + startData.get(i));
                    //startActivity(new Intent(ListOfTerms.this, EditTerm.class));
                    Intent editMentorIntent = new Intent(ListOfMentorsFromAdd.this, EditMentorToAdd.class);
                    //startActivity(editTermIntent);
                    editMentorIntent.putExtra("id", nameId);
                    editMentorIntent.putExtra("name", name);
                    editMentorIntent.putExtra("phone", phoneList.get(i));
                    editMentorIntent.putExtra("email", emailList.get(i));
                    startActivity(editMentorIntent);
                }
                else{
                    toastMessage("No ID associated with that name");
                }
            }
        });
    }

    private void toastMessage(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

}
