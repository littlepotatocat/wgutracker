package com.example.becki.wgutracker;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AddCourse extends AppCompatActivity {

    private static String email = "myself@my.wgu.edu";

    Context context = this;
    String TAG = "AddCourse";
    EditText editDate;
    EditText editDate2;
    CheckBox startAlerts;
    CheckBox endAlerts;
    Calendar myCalendar = Calendar.getInstance();
    Calendar myCalendar2 = Calendar.getInstance();
    String dateFormat = "MM/dd/yyyy";
    DatePickerDialog.OnDateSetListener date;
    DatePickerDialog.OnDateSetListener date2;
    SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.getDefault());
    int courseId;

    EditText editTitle;
    EditText editStatus;
    EditText editNotes;
    Spinner spinner;
    //ListView listView;
    RecyclerView recyclerView;

    DBOpenHelper mDBOpenHelper;

    //ArrayList<Integer> termIds = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_course);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        editTitle = (EditText) findViewById(R.id.editText6);
        editDate = (EditText) findViewById(R.id.editText7);
        editDate2 = (EditText) findViewById(R.id.editText8);
        editStatus = (EditText) findViewById(R.id.editText9);
        editNotes = (EditText) findViewById(R.id.editText10);
        spinner = (Spinner) findViewById(R.id.spinner);
        startAlerts = (CheckBox) findViewById(R.id.checkBox);
        endAlerts = (CheckBox) findViewById(R.id.checkBox2);
        //listView = (ListView) findViewById(android.R.id.list);
        recyclerView = (RecyclerView) findViewById(R.id.list);
        ViewSwitcher mentorSwitcher = findViewById(R.id.switcher);

        mDBOpenHelper = new DBOpenHelper(getApplicationContext());

        Cursor data = mDBOpenHelper.getTermData();
        final Cursor mentors = mDBOpenHelper.getMentorData();
        //Log.d(TAG, "getTermData Worked");
        ArrayList<String> listData = new ArrayList<>();
        final ArrayList<Integer> termIds = new ArrayList<>();
        final List<Mentor> mentorList = new ArrayList<>();


        while(data.moveToNext()) {
            listData.add(data.getString(3));
            termIds.add(data.getInt(0));
        }
        SpinnerAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listData);
        spinner.setAdapter(adapter);
        Log.d(TAG, "Spinner Worked");

        //mentorList.add(new Mentor("a","b","c"));
        while(mentors.moveToNext()) {
            Log.d(TAG, "mentorlist modified");
            mentorList.add(new Mentor(mentors.getString(1),mentors.getString(3),mentors.getString(2), mentors.getInt(0)));
        }

        //replace Recycler View with empty message if size is 0
        if (mentorList.size() > 0) {
            if (R.id.list == mentorSwitcher.getNextView().getId()) {
                mentorSwitcher.showNext();
            }
        MentorRecyclerViewAdapter mentorAdapter = new MentorRecyclerViewAdapter(mentorList);
        recyclerView.setAdapter(mentorAdapter);
        } else if (R.id.text_empty == mentorSwitcher.getNextView().getId()) {
            mentorSwitcher.showNext();
        }
        //recyclerView.
        Log.d(TAG, "RecyclerView Worked");

        //int termIdToGet = termIds.get(spinner.getSelectedItemPosition());
        //final int id2 = termIdToGet;

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        //.setAction("Action", null).show();
                String newCourse = editTitle.getText().toString();
                //int termId = id2;
                int termId = termIds.get(spinner.getSelectedItemPosition());
                String newCourseStart = editDate.getText().toString();
                String newCourseEnd = editDate2.getText().toString();
                String newCourseStatus = editStatus.getText().toString();
                String newCourseNotes = editNotes.getText().toString();
                boolean enableStartAlerts = startAlerts.isChecked();
                boolean enableEndAlerts = endAlerts.isChecked();

                AddCourse(newCourse, newCourseStart, newCourseEnd, newCourseStatus, newCourseNotes, enableStartAlerts, enableEndAlerts, termId);
                for(Mentor m:mentorList) {
                    if (m.isSelected()) {
                        ConnectMentor(m, courseId);
                    }
                }

                Cursor c = mDBOpenHelper.getCourseId(newCourse);
                c.moveToNext();

                SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
                Date dt = new Date();
                Date endDt = new Date();
                try {
                    dt = formatter.parse(newCourseStart);
                    endDt = formatter.parse(newCourseEnd);
                } catch (ParseException e) {

                }
                Calendar cal = Calendar.getInstance();
                Calendar endCal = Calendar.getInstance();
                cal.setTime(dt);
                endCal.setTime(endDt);
                cal.add(Calendar.DATE, -1);
                endCal.add(Calendar.DATE, -1);
                long startDateMillis = cal.getTimeInMillis();
                long endDateMillis = endCal.getTimeInMillis();
                Log.d(TAG, "startDateMillis: " + startDateMillis);

                int reqCode = c.getInt(0);
                if (enableStartAlerts) {
                    //Log.d(TAG, "Went into if(alert)");
                    Intent intent = new Intent(getApplicationContext(), MyReceiver.class);
                    intent.putExtra("title", "Course start reminder!");
                    intent.putExtra("text", "Course " + editTitle.getText() + " begins in one day.");
                    PendingIntent sender = PendingIntent.getBroadcast(AddCourse.this, reqCode, intent, 0);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, startDateMillis, sender);
                }
                if (enableEndAlerts) {
                    //Log.d(TAG, "Went into if(alert)");
                    reqCode = -1*reqCode;
                    Intent intent = new Intent(getApplicationContext(), MyReceiver.class);
                    intent.putExtra("title", "Course end reminder!");
                    intent.putExtra("text", "Course " + editTitle.getText() + " ends in one day.");
                    PendingIntent sender = PendingIntent.getBroadcast(AddCourse.this, reqCode, intent, 0);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, endDateMillis, sender);
                }

                startActivity(new Intent(AddCourse.this, ListOfCourses.class));
                //editText.setText("");
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button addMentor = (Button) findViewById(R.id.button7);
        addMentor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                startActivity(new Intent(AddCourse.this, AddMentor.class));
            }
        });

        Button editMentor = (Button) findViewById(R.id.button8);
        editMentor.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AddCourse.this, ListOfMentorsFromAdd.class));
            }
        });

        Button shareNotes = (Button) findViewById(R.id.button10);
        shareNotes.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //Send Email
                String[] addresses = {email};
                Intent intent =  new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:"));
                //intent.putExtra(Intent.EXTRA_EMAIL, addresses);
                intent.putExtra(Intent.EXTRA_SUBJECT, editTitle.getText() + " Notes");
                intent.putExtra(Intent.EXTRA_TEXT, editNotes.getText());
                Log.d(TAG, "Clicked to send Notes");
                if(intent.resolveActivity(getPackageManager()) != null){
                    Log.d(TAG, "Made it into resolveActivity");
                    startActivity(intent);
                }

            }
        });

        calendarPicker(R.id.editText7);
        calendarPicker2(R.id.editText8);
    }

    public void AddCourse(String title, String start, String end, String status, String notes, boolean startAlert, boolean endAlert, int termId) {
        //boolean insertData = mDBOpenHelper.addCourseData(title, start, end, status, notes, startAlert, endAlert, termId);
        courseId = mDBOpenHelper.addCourseData(title, start, end, status, notes, startAlert, endAlert, termId);
        if (courseId != 0) {
            toastMessage("Data Successfully Inserted");
        } else {
            toastMessage("Something went wrong");
        }
    }

    public void ConnectMentor(Mentor m, int courseId) {
        mDBOpenHelper.ConnectMentor(m.getId(), courseId);

    }

    public void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void calendarPicker(int textboxId){
        editDate = (EditText) findViewById(textboxId);
        long currentdate = System.currentTimeMillis();
        String dateString = sdf.format(currentdate);
        editDate.setText(dateString);

        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDate();
            }

        };

        editDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(context, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    private void calendarPicker2(int textboxId){
        editDate2 = (EditText) findViewById(textboxId);
        long currentdate = System.currentTimeMillis();
        String dateString = sdf.format(currentdate);
        editDate2.setText(dateString);

        date2 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar2.set(Calendar.YEAR, year);
                myCalendar2.set(Calendar.MONTH, monthOfYear);
                myCalendar2.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDate2();
            }

        };

        editDate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(context, date2, myCalendar2.get(Calendar.YEAR), myCalendar2.get(Calendar.MONTH), myCalendar2.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    private void updateDate() {
        editDate.setText(sdf.format(myCalendar.getTime()));
    }

    private void updateDate2() {
        editDate2.setText(sdf.format(myCalendar2.getTime()));
    }
}


