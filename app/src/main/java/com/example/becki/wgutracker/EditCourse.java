package com.example.becki.wgutracker;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class EditCourse extends AppCompatActivity {

    Context context = this;
    String TAG = "EditCourse";
    EditText editDate;
    EditText editDate2;
    CheckBox startAlerts;
    CheckBox endAlerts;
    Calendar myCalendar = Calendar.getInstance();
    Calendar myCalendar2 = Calendar.getInstance();
    String dateFormat = "MM/dd/yyyy";
    DatePickerDialog.OnDateSetListener date;
    DatePickerDialog.OnDateSetListener date2;
    SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.getDefault());
    int courseId;
    EditText editTitle;
    EditText editStatus;
    EditText editNotes;
    Spinner spinner;
    //ListView listView;
    RecyclerView recyclerView;

    DBOpenHelper mDBOpenHelper;

    private String selectedTitle;
    private int selectedId;
    private String selectedStart;
    private String selectedEnd;
    //private boolean selectedStartAlert;
    //private boolean selectedEndAlert;
    private int selectedStartAlert;
    private int selectedEndAlert;
    private boolean selectedStartAlertBoolean;
    private boolean selectedEndAlertBoolean;
    private String selectedStatus;
    private String selectedNotes;
    //Attempt for spinner:
    private int selectedTermId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_course);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        editTitle = (EditText) findViewById(R.id.editText6);
        editDate = (EditText) findViewById(R.id.editText7);
        editDate2 = (EditText) findViewById(R.id.editText8);
        editStatus = (EditText) findViewById(R.id.editText9);
        editNotes = (EditText) findViewById(R.id.editText10);
        spinner = (Spinner) findViewById(R.id.spinner);
        startAlerts = (CheckBox) findViewById(R.id.checkBox);
        endAlerts = (CheckBox) findViewById(R.id.checkBox2);
        //listView = (ListView) findViewById(android.R.id.list);
        recyclerView = (RecyclerView) findViewById(R.id.list);
        ViewSwitcher mentorSwitcher = findViewById(R.id.switcher);

        mDBOpenHelper = new DBOpenHelper(getApplicationContext());

        Intent receivedIntent = getIntent();

        selectedId = receivedIntent.getIntExtra("id", -1);
        selectedTitle = receivedIntent.getStringExtra("title");
        selectedStart = receivedIntent.getStringExtra("start");
        selectedEnd = receivedIntent.getStringExtra("end");
        //selectedStartAlert = receivedIntent.getBooleanExtra("startAlert", false);
        //selectedEndAlert = receivedIntent.getBooleanExtra("endAlert", false);
        selectedStartAlert = receivedIntent.getIntExtra("startAlert", -1);
        selectedEndAlert = receivedIntent.getIntExtra("endAlert", -1);
        selectedStatus = receivedIntent.getStringExtra("status");
        selectedNotes = receivedIntent.getStringExtra("notes");
        selectedTermId = receivedIntent.getIntExtra("termId", -1);
        Log.d(TAG, "Editing course with selected termId: " + selectedTermId);
        editTitle.setText(selectedTitle);
        editDate.setText(selectedStart);
        editDate2.setText(selectedEnd);
        //spinner.setSelection();
        if (selectedStartAlert == 0) {
            selectedStartAlertBoolean = false;
        } else{
            selectedStartAlertBoolean = true;
        }
        startAlerts.setChecked(selectedStartAlertBoolean);
        if (selectedEndAlert == 0) {
            selectedEndAlertBoolean = false;
        } else{
            selectedEndAlertBoolean = true;
        }
        endAlerts.setChecked(selectedEndAlertBoolean);
        editStatus.setText(selectedStatus);
        editNotes.setText(selectedNotes);

        Cursor data = mDBOpenHelper.getTermData();
        final Cursor mentors = mDBOpenHelper.getMentorData(selectedId);
        ArrayList<String> listData = new ArrayList<>();
        final ArrayList<Integer> termIds = new ArrayList<>();
        final List<Mentor> mentorList = new ArrayList<>();

        while(data.moveToNext()) {
            listData.add(data.getString(3));
            termIds.add(data.getInt(0));
        }

        while(mentors.moveToNext()) {
            mentorList.add(new Mentor(mentors.getString(1),mentors.getString(3),mentors.getString(2), mentors.getInt(4)==1, mentors.getInt(0)));
        }

        SpinnerAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listData);
        spinner.setAdapter(adapter);

        //Select choice of Spinner
        spinner.setSelection(termIds.indexOf(selectedTermId));

        //replace Recycler View with empty message if size is 0
        if (mentorList.size() > 0) {
            if (R.id.list == mentorSwitcher.getNextView().getId()) {
                mentorSwitcher.showNext();
            }

            MentorRecyclerViewAdapter mentorAdapter = new MentorRecyclerViewAdapter(mentorList);
            recyclerView.setAdapter(mentorAdapter);

        } else if (R.id.text_empty == mentorSwitcher.getNextView().getId()) {
            mentorSwitcher.showNext();
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String courseTitle = editTitle.getText().toString();
                String courseStart = editDate.getText().toString();
                String courseEnd = editDate2.getText().toString();
                boolean startChecked = startAlerts.isChecked();
                int startCheckedInt;
                if (startChecked == false) {
                    startCheckedInt = 0;
                } else{
                    startCheckedInt = 1;
                }
                boolean endChecked = endAlerts.isChecked();
                int endCheckedInt;
                if (endChecked == false){
                    endCheckedInt = 0;
                } else{
                    endCheckedInt = 1;
                }
                String courseStatus = editStatus.getText().toString();
                String courseNotes = editNotes.getText().toString();
                //Placeholder
                int termId = termIds.get(spinner.getSelectedItemPosition());

                mDBOpenHelper.updateCourse(courseTitle, selectedId, selectedTitle, courseStart, courseEnd, startCheckedInt, endCheckedInt, courseStatus, courseNotes, termId);
                mDBOpenHelper.updateCourseMentors(selectedId, mentorList);

                SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
                Date dt = new Date();
                Date endDt = new Date();
                try {
                    dt = formatter.parse(courseStart);
                    endDt = formatter.parse(courseEnd);
                } catch (ParseException e) {

                }
                Calendar cal = Calendar.getInstance();
                Calendar endCal = Calendar.getInstance();
                cal.setTime(dt);
                endCal.setTime(endDt);
                cal.add(Calendar.DATE, -1);
                endCal.add(Calendar.DATE, -1);
                long startDateMillis = cal.getTimeInMillis();
                long endDateMillis = endCal.getTimeInMillis();
                Log.d(TAG, "startDateMillis: " + startDateMillis);

                int reqCode = selectedId;
                if (startChecked) {
                    //Log.d(TAG, "Went into if(alert)");
                    Intent intent = new Intent(getApplicationContext(), MyReceiver.class);
                    intent.putExtra("title", "Course start reminder!");
                    intent.putExtra("text", "Course " + courseTitle + " begins in one day.");
                    PendingIntent sender = PendingIntent.getBroadcast(EditCourse.this, reqCode, intent, 0);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, startDateMillis, sender);
                }else{
                    Intent intent = new Intent(getApplicationContext(), MyReceiver.class);
                    PendingIntent sender = PendingIntent.getBroadcast(EditCourse.this, reqCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    alarmManager.cancel(sender);
                }
                if (endChecked) {
                    //Log.d(TAG, "Went into if(alert)");
                    reqCode = -1*reqCode;
                    Intent intent = new Intent(getApplicationContext(), MyReceiver.class);
                    intent.putExtra("title", "Course end reminder!");
                    intent.putExtra("text", "Course " + courseTitle + " ends in one day.");
                    PendingIntent sender = PendingIntent.getBroadcast(EditCourse.this, reqCode, intent, 0);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, endDateMillis, sender);
                }else{
                    reqCode = -1*reqCode;
                    Intent intent = new Intent(getApplicationContext(), MyReceiver.class);
                    PendingIntent sender = PendingIntent.getBroadcast(EditCourse.this, reqCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    alarmManager.cancel(sender);
                }


                startActivity(new Intent(EditCourse.this, ListOfCourses.class));
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Button addMentor = (Button) findViewById(R.id.button7);
        addMentor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                startActivity(new Intent(EditCourse.this, AddMentorFromEditCourse.class));
            }
        });

        Button editMentor = (Button) findViewById(R.id.button8);
        editMentor.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EditCourse.this, ListOfMentorsFromEdit.class));
            }
        });

        Button shareNotes = (Button) findViewById(R.id.button10);
        shareNotes.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //Send Email
                Intent intent =  new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:"));
                intent.putExtra(Intent.EXTRA_SUBJECT, editTitle.getText() + " Notes");
                intent.putExtra(Intent.EXTRA_TEXT, editNotes.getText());
                if(intent.resolveActivity(getPackageManager()) != null){
                    startActivity(intent);
                }

            }
        });

        calendarPicker(R.id.editText7);
        calendarPicker2(R.id.editText8);
    }

    public void ConnectMentor(Mentor m, int courseId) {
        mDBOpenHelper.ConnectMentor(m.getId(), courseId);

    }

    public void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void calendarPicker(int textboxId){
        editDate = (EditText) findViewById(textboxId);
        //long currentdate = System.currentTimeMillis();
        //String dateString = sdf.format(currentdate);
        //editDate.setText(dateString);

        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDate();
            }

        };

        editDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(context, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    private void calendarPicker2(int textboxId){
        editDate2 = (EditText) findViewById(textboxId);
        //long currentdate = System.currentTimeMillis();
        //String dateString = sdf.format(currentdate);
        //editDate2.setText(dateString);

        date2 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar2.set(Calendar.YEAR, year);
                myCalendar2.set(Calendar.MONTH, monthOfYear);
                myCalendar2.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDate2();
            }

        };

        editDate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(context, date2, myCalendar2.get(Calendar.YEAR), myCalendar2.get(Calendar.MONTH), myCalendar2.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    private void updateDate() {
        editDate.setText(sdf.format(myCalendar.getTime()));
    }

    private void updateDate2() {
        editDate2.setText(sdf.format(myCalendar2.getTime()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_edit_screens, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                Log.d(TAG, "Clicked Back Arrow Successfully");
                return true;

            case R.id.action_delete:
                Log.d(TAG, "Clicked Trash Successfully");
                mDBOpenHelper.deleteCourse(selectedId);
                startActivity(new Intent(EditCourse.this, ListOfCourses.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
