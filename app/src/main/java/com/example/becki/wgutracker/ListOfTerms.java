package com.example.becki.wgutracker;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListOfTerms extends AppCompatActivity {

    private ListView mListView;
    DBOpenHelper mDBOpenHelper;
    String TAG = "ListOfTerms";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_terms);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ListOfTerms.this, AddTerm.class));
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //mListView = (ListView) findViewById(R.id.);

        // Create the "terms" string array
        //String[] terms = new String[]{"C169", "C188", "C196",
        //        "C482", "EDV1", "TXC1", "TXP1", "TYC1", "TYP1"};

        // Create the ArrayAdapter using our "courses" string array.
        // The "android.R.layout.simple_list_item_1" defines the button layout.
        //ListAdapter termAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, terms);

        // Pass our adapter to the ListView (we extended the ListActivity class)
        //setListAdapter(termAdapter);

        // Here we are simply creating a setOnItemClickListener so we can generate
        // a toast when a button is clicked.
        //getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            //public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //String s = ((TextView) view).getText() + " is in array position " + position;
                //Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
            //}
        //});
        mListView = (ListView) findViewById(android.R.id.list);
        mDBOpenHelper = new DBOpenHelper(this);
        populateListView();
        //editItem();
    }

    private void populateListView() {
        Cursor data = mDBOpenHelper.getTermData();
        //Log.d(TAG, "getTermData Worked");
        ArrayList<String> listData = new ArrayList<>();
        final ArrayList<String> startData = new ArrayList<>();
        final ArrayList<String> endData = new ArrayList<>();

        while(data.moveToNext()) {
            listData.add(data.getString(3));
            startData.add(data.getString(1));
            endData.add(data.getString(2));
        }

        ListAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listData);
        mListView.setAdapter(adapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String title = adapterView.getItemAtPosition(i).toString();
                Log.d(TAG, "onItemClick: You clicked on " + title);

                Cursor data = mDBOpenHelper.getTermId(title);
                int itemId = -1;
                while (data.moveToNext()){
                    itemId = data.getInt(0);
                }
                if(itemId > -1) {
                    Log.d(TAG, "The id is: " + itemId);
                    Log.d(TAG, "The start is: " + startData.get(i));
                    //startActivity(new Intent(ListOfTerms.this, EditTerm.class));
                    Intent editTermIntent = new Intent(ListOfTerms.this, EditTerm.class);
                    //startActivity(editTermIntent);
                    editTermIntent.putExtra("id", itemId);
                    editTermIntent.putExtra("title", title);
                    editTermIntent.putExtra("start", startData.get(i));
                    editTermIntent.putExtra("end", endData.get(i));
                    startActivity(editTermIntent);
                }
                else{
                    toastMessage("No ID associated with that name");
                }
            }
        });

    }
    private void toastMessage(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

}
