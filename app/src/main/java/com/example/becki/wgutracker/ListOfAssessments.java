package com.example.becki.wgutracker;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListOfAssessments extends AppCompatActivity {

    private ListView mListView;
    DBOpenHelper mDBOpenHelper;
    String TAG = "ListOfAssessments";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_assessments);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ListOfAssessments.this, AddAssessment.class));
            }
        });

        mListView = (ListView) findViewById(android.R.id.list);
        mDBOpenHelper = new DBOpenHelper(this);
        populateListView();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void populateListView() {

        Cursor assessments = mDBOpenHelper.getAssessmentData();
        ArrayList<String> assessmentData = new ArrayList<>();
        final ArrayList<String> goalDates = new ArrayList<>();
        final ArrayList<String> dueDates = new ArrayList<>();
        final ArrayList<Boolean> alerts = new ArrayList<>();
        final ArrayList<Boolean> isPerfAssess = new ArrayList<>();
        final ArrayList<Boolean> isObjAssess = new ArrayList<>();
        final ArrayList<Integer> courseId = new ArrayList<>();

        while(assessments.moveToNext()){
            assessmentData.add(assessments.getString(2));
            goalDates.add(assessments.getString(4));
            dueDates.add(assessments.getString(3));
            alerts.add(assessments.getInt(5)==1);
            isPerfAssess.add(assessments.getInt(6)==1);
            isObjAssess.add(assessments.getInt(6)!=1);
            courseId.add(assessments.getInt(1));
        }

        ListAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, assessmentData);
        //setListAdapter(adapter);
        mListView.setAdapter(adapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String title = adapterView.getItemAtPosition(i).toString();
                Log.d(TAG, "onItemClick: You clicked on " + title);

                Cursor data = mDBOpenHelper.getAssessmentId(title);
                int assessId = -1;
                while (data.moveToNext()){
                    assessId = data.getInt(0);
                }
                if(assessId > -1) {
                    //Log.d(TAG, "The id is: " + itemId);
                    //Log.d(TAG, "The start is: " + startData.get(i));
                    //startActivity(new Intent(ListOfTerms.this, EditTerm.class));
                    Intent editAssessmentIntent = new Intent(ListOfAssessments.this, EditAssessment.class);
                    //startActivity(editTermIntent);
                    editAssessmentIntent.putExtra("id", assessId);
                    editAssessmentIntent.putExtra("title", title);
                    editAssessmentIntent.putExtra("goal", goalDates.get(i));
                    editAssessmentIntent.putExtra("due", dueDates.get(i));
                    editAssessmentIntent.putExtra("alert", alerts.get(i));
                    editAssessmentIntent.putExtra("isPerf", isPerfAssess.get(i));
                    editAssessmentIntent.putExtra("isObj", isObjAssess.get(i));
                    editAssessmentIntent.putExtra("courseId", courseId.get(i));
                    startActivity(editAssessmentIntent);
                }
                else{
                    toastMessage("No ID associated with that name");
                }
            }
        });

    }
    private void toastMessage(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }


}
