package com.example.becki.wgutracker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DBOpenHelper extends SQLiteOpenHelper{
/*
    public static final String DATABASE_NAME = "notes.db";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_NOTES = "term";
    public static final String NOTE_ID = "_termId";
    public static final String NOTE_TEXT = "startDate";
    public static final String NOTE_CREATED = "endDate";
    //public static final String NOTE_CREATED = "termTitle";

    private static final String TABLE_CREATE = "CREATE TABLE " + TABLE_NOTES + " (" + NOTE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + NOTE_TEXT + " TEXT, " + NOTE_CREATED + " TEXT default CURRENT_TIMESTAMP" + ")";
*/
    public static final String DATABASE_NAME = "wgu.db";
    private static final String TAG = "DBOpenHelper";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_TERM = "term";
    public static final String TERM_ID = "_termId";
    public static final String TERM_START = "startDate";
    public static final String TERM_END = "endDate";
    public static final String TERM_TITLE = "termTitle";

    public static final String TABLE_COURSE = "course";
    public static final String COURSE_ID = "_courseId";
    public static final String COURSE_START = "courseStartDate";
    public static final String COURSE_END = "courseEndDate";
    public static final String COURSE_TITLE = "courseTitle";
    public static final String COURSE_STATUS = "courseStatus";
    public static final String COURSE_START_ALERT = "courseStartAlert";
    public static final String COURSE_END_ALERT = "courseEndAlert";
    public static final String COURSE_NOTES = "courseNotes";

    public static final String TABLE_COURSE_ASSESSMENTS = "courseAssessments";
    public static final String COURSEASSESSMENTS_ID = "_courseAssessmentsId";

    public static final String TABLE_COURSE_MENTORS = "courseMentors";
    public static final String COURSEMENTORS_ID = "_courseMentorsId";

    public static final String TABLE_ASSESSMENT = "assessment";
    public static final String ASSESSMENT_ID = "_assessmentId";
    public static final String ASSESSMENT_TITLE = "assessmentTitle";
    public static final String ASSESSMENT_DUE = "assessmentDue";
    public static final String ASSESSMENT_GOAL = "assessmentGoal";
    public static final String ASSESSMENT_ALERT = "assessmentAlert";
    public static final String ASSESSMENT_PERFORMANCE = "assessmentPerformance";

    public static final String TABLE_MENTOR = "mentor";
    public static final String MENTOR_ID = "_mentorId";
    public static final String MENTOR_NAME = "mentorName";
    public static final String MENTOR_PHONE = "mentorPhone";
    public static final String MENTOR_EMAIL = "mentorEmail";

    private static final String TERM_TABLE_CREATE = " CREATE TABLE " + TABLE_TERM + " (" +
            TERM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            TERM_START + " TEXT, " + TERM_END + " TEXT," +
            TERM_TITLE + " TEXT);";
    private static final String COURSE_TABLE_CREATE = " CREATE TABLE " + TABLE_COURSE + " (" +
            COURSE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            TERM_ID + " INTEGER, "  +
            COURSE_START + " TEXT, " +
            COURSE_END + " TEXT," +
            COURSE_TITLE + " TEXT, " +
            COURSE_STATUS + " TEXT, " +
            COURSE_NOTES + " TEXT, " +
            COURSE_START_ALERT + " TEXT, " +
            COURSE_END_ALERT + " TEXT, " +
            "FOREIGN KEY(" + TERM_ID + ") REFERENCES " + TABLE_TERM + "(" + TERM_ID + "));";
    private static final String ASSESSMENT_TABLE_CREATE = " CREATE TABLE " + TABLE_ASSESSMENT + " (" +
            ASSESSMENT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COURSE_ID + " INTEGER, " +
            ASSESSMENT_TITLE + " TEXT, " +
            ASSESSMENT_DUE + " TEXT, " +
            ASSESSMENT_GOAL + " TEXT, " +
            ASSESSMENT_ALERT + " TEXT, " +
            ASSESSMENT_PERFORMANCE + " TEXT, " +
            "FOREIGN KEY(" + COURSE_ID + ") REFERENCES " + TABLE_COURSE + "(" + COURSE_ID + "));";
    private static final String MENTOR_TABLE_CREATE = " CREATE TABLE " + TABLE_MENTOR + " (" +
            MENTOR_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            MENTOR_NAME + " TEXT, " +
            MENTOR_PHONE + " TEXT, " +
            MENTOR_EMAIL + " TEXT)";
    private static final String COURSEMENTORS_TABLE_CREATE = "CREATE TABLE " + TABLE_COURSE_MENTORS + " (" +
            COURSEMENTORS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COURSE_ID + " INTEGER, " +
            MENTOR_ID + " INTEGER, " +
            "FOREIGN KEY(" + COURSE_ID + ") REFERENCES " + TABLE_COURSE + "(" + COURSE_ID + "), " +
            "FOREIGN KEY(" + MENTOR_ID + ") REFERENCES " + TABLE_MENTOR + "(" + MENTOR_ID + "));";
    private static final String COURSEASSESSMENTS_TABLE_CREATE = "CREATE TABLE " + TABLE_COURSE_ASSESSMENTS + " (" +
            COURSEASSESSMENTS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COURSE_ID + " INTEGER, " +
            ASSESSMENT_ID + " INTEGER, " +
            "FOREIGN KEY(" + COURSE_ID + ") REFERENCES " + TABLE_COURSE + "(" + COURSE_ID + "), " +
            "FOREIGN KEY(" + ASSESSMENT_ID + ") REFERENCES " + TABLE_ASSESSMENT + "(" + ASSESSMENT_ID + "));";

    public DBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TERM_TABLE_CREATE);
        Log.d(TAG, "Term Tables newly Created");
        db.execSQL(COURSE_TABLE_CREATE);
        Log.d(TAG, "Course Tables newly Created");
        db.execSQL(ASSESSMENT_TABLE_CREATE);
        Log.d(TAG, "Assessment Tables newly Created");
        db.execSQL(MENTOR_TABLE_CREATE);
        Log.d(TAG, "Mentor Tables newly Created");
        db.execSQL(COURSEMENTORS_TABLE_CREATE);
        Log.d(TAG, "Course Mentors Tables newly Created");
        db.execSQL(COURSEASSESSMENTS_TABLE_CREATE);
        Log.d(TAG, "Course Assessments Tables newly Created");
        Log.d(TAG, "ALL Tables newly Created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TERM);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COURSE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ASSESSMENT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MENTOR);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COURSE_ASSESSMENTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COURSE_MENTORS);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TERM);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COURSE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ASSESSMENT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MENTOR);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COURSE_ASSESSMENTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COURSE_MENTORS);
        onCreate(db);
    }

    public boolean addTermData(String item, String sDate, String eDate) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TERM_TITLE, item);
        contentValues.put(TERM_START, sDate);
        contentValues.put(TERM_END, eDate);

        Log.d(TAG, "addTermData: Adding " + item + " to " + TABLE_TERM);

        long result = db.insert(TABLE_TERM, null, contentValues);
        db.close();
        if (result == -1) {
            Log.d(TAG, "addTermData: failure");
            return false;
        } else {
            Log.d(TAG, "addTermData: success");
            return true;
        }
    }

    public Cursor getTermData(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_TERM + ";";
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    public Cursor getTermId(String title){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + TERM_ID + " FROM " + TABLE_TERM + " WHERE " + TERM_TITLE + " = '" + title + "'";
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    public void updateTerm(String newTitle, int id, String oldTitle, String termStart, String termEnd) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_TERM + " SET " + TERM_TITLE + " = '" + newTitle +
                "', " + TERM_START + " = '" + termStart + "', " + TERM_END + " = '" + termEnd +
                "' WHERE " + TERM_ID + " = '" +
                id + "' AND " + TERM_TITLE + " = '" + oldTitle + "'";
        Log.d(TAG, "UpdateTerm: " + query);
        Log.d(TAG, "UpdateTerm: " + newTitle);
        db.execSQL(query);
    }

    public boolean deleteTerm(int termId){
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_COURSE + " WHERE " + TERM_ID + " = " + termId + ";";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (!cursor.moveToNext()){
            String query = "DELETE FROM " + TABLE_TERM + " WHERE " + TERM_ID + " = " + termId + ";";
            db.execSQL(query);
            return true;
        }
        else{
            return false;
        }

    }

    public int addCourseData(String title, String sDate, String eDate, String status, String notes, boolean startAlert, boolean endAlert, int termId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COURSE_TITLE, title);
        contentValues.put(COURSE_START, sDate);
        contentValues.put(COURSE_END, eDate);
        contentValues.put(COURSE_STATUS, status);
        contentValues.put(COURSE_NOTES, notes);
        contentValues.put(COURSE_START_ALERT, startAlert);
        contentValues.put(COURSE_END_ALERT, endAlert);
        contentValues.put(TERM_ID, termId);

        Log.d(TAG, "addCourseData: Adding " + title + " to " + TABLE_COURSE);

        long result = db.insert(TABLE_COURSE, null, contentValues);
        if (result == -1) {
            Log.d(TAG, "addCourseData: failure");

        } else {
            Log.d(TAG, "addCourseData: success");

        }
        String query = "SELECT last_insert_rowid()";
        Cursor c = db.rawQuery(query, null);
        c.moveToNext();
        int courseId = c.getInt(0);
        db.close();

        if (courseId == 0) {
            Log.d(TAG, "getCourseId: failure");

        } else {
            Log.d(TAG, "getCourseId: success");

        }
        return courseId;
    }

    public Cursor getCourseData(){
        SQLiteDatabase db = this.getWritableDatabase();
        //String query = "SELECT * FROM " + TABLE_COURSE + ";";
        String query = "SELECT _courseId, course._termId, courseStartDate, courseEndDate,courseTitle, courseStatus, courseNotes, courseStartAlert, courseEndAlert, termTitle " +
                "FROM course left outer join term on course._termId = term._termId;";
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    public Cursor getCourseId(String title){
        SQLiteDatabase db = this.getWritableDatabase();
        title = title.replace("'", "''" );
        String query = "SELECT " + COURSE_ID + " FROM " + TABLE_COURSE + " WHERE " + COURSE_TITLE + " = '" + title + "'";
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    public void updateCourse(String newTitle, int id, String oldTitle, String start, String end, int sAlert, int eAlert, String status, String notes, int termId) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_COURSE + " SET " + COURSE_TITLE + " = '" + newTitle +
                "', " + COURSE_START + " = '" + start + "', " + COURSE_END + " = '" + end +
                "', " + COURSE_START_ALERT + " = '" + sAlert + "', " + COURSE_END_ALERT + " = '" + eAlert +
                "', " + COURSE_STATUS + " = '" + status + "', " + COURSE_NOTES + " = '" + notes +
                "', " + TERM_ID + " = '" + termId +
                "' WHERE " + COURSE_ID + " = '" +
                id + "' AND " + COURSE_TITLE + " = '" + oldTitle + "'";
        Log.d(TAG, "UpdateCourse: " + query);
        Log.d(TAG, "UpdateCourse: " + newTitle);
        db.execSQL(query);
    }

    public void deleteCourse(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_COURSE + " WHERE " + COURSE_ID + " = " + id + ";";
        db.execSQL(query);
    }

    public boolean addMentorData(String name, String phone, String email) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MENTOR_NAME, name);
        contentValues.put(MENTOR_PHONE, phone);
        contentValues.put(MENTOR_EMAIL, email);

        Log.d(TAG, "addMentorData: Adding " + name + " to " + TABLE_MENTOR);

        long result = db.insert(TABLE_MENTOR, null, contentValues);
        db.close();
        if (result == -1) {
            Log.d(TAG, "addMentorData: failure");
            return false;
        } else {
            Log.d(TAG, "addMentorData: success");
            return true;
        }
    }

    public Cursor getMentorData(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_MENTOR + ";";
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    public Cursor getMentorData(int courseId){
        SQLiteDatabase db = this.getWritableDatabase();
        //String query = "SELECT * FROM " + TABLE_MENTOR + ";";
        String query = "SELECT " + TABLE_MENTOR + "." + MENTOR_ID + ", " +
        MENTOR_NAME + ", " + MENTOR_PHONE + ", " + MENTOR_EMAIL + ", " +
        TABLE_COURSE_MENTORS + "." + COURSE_ID + " is not null as checked FROM " +
        TABLE_MENTOR + " LEFT OUTER JOIN " + TABLE_COURSE_MENTORS + " on " +
        TABLE_MENTOR + "." + MENTOR_ID + " = " + TABLE_COURSE_MENTORS + "." + MENTOR_ID +
        " and " + TABLE_COURSE_MENTORS + "." + COURSE_ID + " = " + courseId + ";";

        Cursor data = db.rawQuery(query, null);
        return data;
    }

    public void updateCourseMentors(int courseId, List<Mentor> mentorList) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_COURSE_MENTORS + " WHERE " + COURSE_ID + " = " + courseId + ";";
        db.execSQL(query);
        for(Mentor m : mentorList){
            if (m.isSelected()){
                ConnectMentor(m.getId(), courseId);
            }
        }
    }

    public boolean ConnectMentor(int mentorId, int courseId){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MENTOR_ID, mentorId);
        contentValues.put(COURSE_ID, courseId);

        long result = db.insert(TABLE_COURSE_MENTORS, null, contentValues);
        db.close();
        if (result == -1) {
            Log.d(TAG, "ConnectingMentorData: failure");
            return false;
        } else {
            Log.d(TAG, "ConnectingMentorData: success");
            return true;
        }
    }

    public Cursor getMentorId(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + MENTOR_ID + " FROM " + TABLE_MENTOR + " WHERE " + MENTOR_NAME + " = '" + name + "'";
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    public void updateMentor(String newName, int id, String oldName, String phone, String email) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_MENTOR + " SET " + MENTOR_NAME + " = '" + newName +
                "', " + MENTOR_PHONE + " = '" + phone + "', " + MENTOR_EMAIL + " = '" + email +
                "' WHERE " + MENTOR_ID + " = '" +
                id + "' AND " + MENTOR_NAME + " = '" + oldName + "'";
        Log.d(TAG, "UpdateMentor: " + query);
        Log.d(TAG, "UpdateMentor: " + newName);
        db.execSQL(query);
    }

    public void deleteMentor(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_MENTOR + " WHERE " + MENTOR_ID + " = " + id + ";";
        db.execSQL(query);
    }

    public boolean addAssessmentData(String title, int courseId, String due, String goal, boolean alert, boolean performance) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ASSESSMENT_TITLE, title);
        contentValues.put(COURSE_ID, courseId);
        contentValues.put(ASSESSMENT_DUE, due);
        contentValues.put(ASSESSMENT_GOAL, goal);
        contentValues.put(ASSESSMENT_ALERT, alert);
        contentValues.put(ASSESSMENT_PERFORMANCE, performance);

        Log.d(TAG, "addTermData: Adding " + title + " to " + TABLE_ASSESSMENT);

        long result = db.insert(TABLE_ASSESSMENT, null, contentValues);
        db.close();
        if (result == -1) {
            Log.d(TAG, "addAssessmentData: failure");
            return false;
        } else {
            Log.d(TAG, "addassessmentData: success");
            return true;
        }
    }

    public Cursor getAssessmentData(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_ASSESSMENT + ";";
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    public Cursor getAssessmentId(String title){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + ASSESSMENT_ID + " FROM " + TABLE_ASSESSMENT + " WHERE " + ASSESSMENT_TITLE + " = '" + title + "'";
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    public void updateAssessment(String newTitle, int id, String oldTitle, String goal, String due, boolean alert, boolean performance, int courseId) {
        SQLiteDatabase db = this.getWritableDatabase();
        int iAlert;
        int iPerformance;
        if (alert == true){
            iAlert = 1;
        }else{
            iAlert = 0;
        }
        if (performance == true) {
            iPerformance = 1;
        }else{
            iPerformance = 0;
        }
        String query = "UPDATE " + TABLE_ASSESSMENT + " SET " + ASSESSMENT_TITLE + " = '" + newTitle +
                "', " + ASSESSMENT_GOAL + " = '" + goal + "', " + ASSESSMENT_DUE + " = '" + due +
                "', " + ASSESSMENT_ALERT + " = '" + iAlert + "', " + ASSESSMENT_PERFORMANCE + " = '" + iPerformance +
                "', " + COURSE_ID + " = '" + courseId +
                "' WHERE " + ASSESSMENT_ID + " = '" +
                id + "' AND " + ASSESSMENT_TITLE + " = '" + oldTitle + "'";
        Log.d(TAG, "UpdateAssessment: " + query);
        Log.d(TAG, "UpdateAssessment: " + newTitle);
        db.execSQL(query);
    }

    public void deleteAssessment(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_ASSESSMENT + " WHERE " + ASSESSMENT_ID + " = " + id + ";";
        db.execSQL(query);
    }
}

