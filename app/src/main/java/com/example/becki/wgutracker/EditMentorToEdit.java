package com.example.becki.wgutracker;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class EditMentorToEdit extends AppCompatActivity {

    EditText editName;
    EditText editPhone;
    EditText editEmail;
    DBOpenHelper mDBOpenHelper;
    String TAG = "EditMentorToEdit";

    private String selectedName;
    private int selectedId;
    private String selectedPhone;
    private String selectedEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_mentor_to_edit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        editName = (EditText) findViewById(R.id.editTitle);
        editPhone = (EditText) findViewById(R.id.editText5);
        editEmail = (EditText) findViewById(R.id.editText11);
        mDBOpenHelper = new DBOpenHelper(getApplicationContext());

        Intent receivedIntent = getIntent();

        selectedId = receivedIntent.getIntExtra("id", -1);
        selectedName = receivedIntent.getStringExtra("name");
        selectedPhone = receivedIntent.getStringExtra("phone");
        selectedEmail = receivedIntent.getStringExtra("email");

        editName.setText(selectedName);
        editPhone.setText(selectedPhone);
        editEmail.setText(selectedEmail);
        Log.d(TAG, "Loaded EditMentorToEdit");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mentorName = editName.getText().toString();
                String mentorPhone = editPhone.getText().toString();
                String mentorEmail = editEmail.getText().toString();
                mDBOpenHelper.updateMentor(mentorName, selectedId, selectedName, mentorPhone, mentorEmail);
                startActivity(new Intent(EditMentorToEdit.this, ListOfMentorsFromEdit.class));
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //Log.d(TAG, "Loaded EditMentorToEdit");
    }

    public void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_edit_screens, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                Log.d(TAG, "Clicked Back Arrow Successfully");
                return true;

            case R.id.action_delete:
                Log.d(TAG, "Clicked Trash Successfully");
                mDBOpenHelper.deleteMentor(selectedId);
                startActivity(new Intent(EditMentorToEdit.this, ListOfMentorsFromEdit.class));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
