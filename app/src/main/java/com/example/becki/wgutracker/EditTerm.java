package com.example.becki.wgutracker;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class EditTerm extends AppCompatActivity {

    Context context = this;
    String TAG = "EditTerm";
    EditText editText;
    EditText editDate;
    EditText editDate2;
    Calendar myCalendar = Calendar.getInstance();
    Calendar myCalendar2 = Calendar.getInstance();
    String dateFormat = "MM/dd/yyyy";
    DatePickerDialog.OnDateSetListener date;
    DatePickerDialog.OnDateSetListener date2;
    SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.getDefault());
    DBOpenHelper mDBOpenHelper;

    private String selectedTitle;
    private int selectedId;
    private String selectedStart;
    private String selectedEnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_term);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        editText = (EditText) findViewById(R.id.editText);
        editDate = (EditText) findViewById(R.id.editText2);
        editDate2 = (EditText) findViewById(R.id.editText3);
        mDBOpenHelper = new DBOpenHelper(getApplicationContext());

        Intent receivedIntent = getIntent();

        selectedId = receivedIntent.getIntExtra("id", -1);
        selectedTitle = receivedIntent.getStringExtra("title");
        selectedStart = receivedIntent.getStringExtra("start");
        selectedEnd = receivedIntent.getStringExtra("end");

        editText.setText(selectedTitle);
        editDate.setText(selectedStart);
        editDate2.setText(selectedEnd);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String termTitle = editText.getText().toString();
                String termStart = editDate.getText().toString();
                String termEnd = editDate2.getText().toString();

                if(!termTitle.equals("")){
                    mDBOpenHelper.updateTerm(termTitle, selectedId, selectedTitle, termStart, termEnd);
                }
                else{
                    toastMessage("You must enter a name.");
                }

                startActivity(new Intent(EditTerm.this, ListOfTerms.class));
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        calendarPicker(R.id.editText2);
        calendarPicker2(R.id.editText3);
    }

    public void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void calendarPicker(int textboxId){
        editDate = (EditText) findViewById(textboxId);

        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDate();
            }

        };

        editDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(context, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void calendarPicker2(int textboxId){
        editDate2 = (EditText) findViewById(textboxId);

        date2 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar2.set(Calendar.YEAR, year);
                myCalendar2.set(Calendar.MONTH, monthOfYear);
                myCalendar2.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDate2();
            }

        };

        editDate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(context, date2, myCalendar2.get(Calendar.YEAR), myCalendar2.get(Calendar.MONTH), myCalendar2.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void updateDate() {
        editDate.setText(sdf.format(myCalendar.getTime()));
    }
    private void updateDate2() {
        editDate2.setText(sdf.format(myCalendar2.getTime()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_edit_screens, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                Log.d(TAG, "Clicked Back Arrow Successfully");
                return true;

            case R.id.action_delete:
                Log.d(TAG, "Clicked Trash Successfully");
                boolean worked = mDBOpenHelper.deleteTerm(selectedId);
                if (worked != true){
                    Toast.makeText(this, "Could not delete the term because it contained a course. Please remove the course first.", Toast.LENGTH_LONG).show();
                }
                startActivity(new Intent(EditTerm.this, ListOfTerms.class));
                return true;
        }

       return super.onOptionsItemSelected(item);
    }
}