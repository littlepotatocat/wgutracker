package com.example.becki.wgutracker;

public class Mentor {
    private String name;

    private String email;

    private String phone;

    private int id;

    private boolean isSelected;

    public Mentor() {
    }

    public Mentor(String name, String email, String phone, int id) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.id = id;
    }

    public Mentor(String name, String email, String phone, boolean isSelected, int id) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.isSelected = isSelected;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
