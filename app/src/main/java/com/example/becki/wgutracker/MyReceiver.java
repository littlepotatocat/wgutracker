package com.example.becki.wgutracker;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.Random;

public class MyReceiver extends BroadcastReceiver {

    static int notificationId;
    String channel_id = "test";
    String TAG = "MyReceiver";

    @Override
    public void onReceive(Context context,Intent intent){
        Log.d(TAG, "Went into MyReceiver!");
        //Toast.makeText(context, "Notification", Toast.LENGTH_LONG).show();
        createNotificationChannel(context, channel_id);
        Notification n = new NotificationCompat.Builder(context, channel_id).setSmallIcon(R.drawable.ic_launcher_foreground).setContentText(intent.getStringExtra("text")).setContentTitle(intent.getStringExtra("title")).build();
        //Notification n = new Notification.Builder(context).setSmallIcon(R.drawable.ic_launcher_foreground).setChannelId(channel_id).setContentTitle("Test Notification with an id of: " + Integer.toString(notificationId)).setContentText("This is a test").build();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationId++, n);
        Log.d(TAG, "Went into MyReceiver!");

    }

    private void createNotificationChannel(Context context, String CHANNEL_ID){
        Log.d(TAG, "Went into createNotificationChannel!");
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "notification channel";
            String description = "notification channel description";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);

            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
            Log.d(TAG, "Went into createNotificationChannel!");
        }
    }

}
