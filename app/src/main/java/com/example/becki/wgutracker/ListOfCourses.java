package com.example.becki.wgutracker;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListOfCourses extends AppCompatActivity {

    private ListView mListView;
    DBOpenHelper mDBOpenHelper;
    String TAG = "ListOfCourses";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_courses);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        //.setAction("Action", null).show();
                startActivity(new Intent(ListOfCourses.this, AddCourse.class));
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mListView = (ListView) findViewById(android.R.id.list);
        mDBOpenHelper = new DBOpenHelper(this);
        populateListView();
    }

    private void populateListView() {

        Cursor courses = mDBOpenHelper.getCourseData();
        ArrayList<String> courseData = new ArrayList<>();
        final ArrayList<String> courseNames = new ArrayList<>();
        final ArrayList<String> startDates = new ArrayList<>();
        final ArrayList<String> endDates = new ArrayList<>();
        //final ArrayList<Boolean> startAlerts = new ArrayList<>();
        //final ArrayList<Boolean> endAlerts = new ArrayList<>();
        final ArrayList<Integer> startAlerts = new ArrayList<>();
        final ArrayList<Integer> endAlerts = new ArrayList<>();
        final ArrayList<String> statusList = new ArrayList<>();
        final ArrayList<String> notesList = new ArrayList<>();
        final ArrayList<Integer> termList = new ArrayList<>();

        while(courses.moveToNext()){
            courseData.add(courses.getString(4) + "  (" + courses.getString(9) + ")");
            courseNames.add(courses.getString(4));
            startDates.add(courses.getString(2));
            endDates.add(courses.getString(3));
            startAlerts.add(courses.getInt(7));
            endAlerts.add(courses.getInt(8));
            statusList.add(courses.getString(5));
            notesList.add(courses.getString(6));
            termList.add(courses.getInt(1));
        }

        ListAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, courseData);
        mListView.setAdapter(adapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                //String title = adapterView.getItemAtPosition(i).toString();
                String title = courseNames.get(i);
                //String start = startDates.get(i);
                Log.d(TAG, "onItemClick: You clicked on " + courseNames.get(i));

                Cursor data = mDBOpenHelper.getCourseId(title);
                int courseId = -1;
                while (data.moveToNext()){
                    courseId = data.getInt(0);
                }
                if(courseId > -1) {
                    Log.d(TAG, "The id is: " + courseId);
                    Log.d(TAG, "The name is: " + courseNames.get(i));
                    //Log.d(TAG, "True or False for Start Check: " + startAlerts.get(i));
                    //startActivity(new Intent(ListOfTerms.this, EditTerm.class));
                    Intent editCourseIntent = new Intent(ListOfCourses.this, EditCourse.class);
                    //startActivity(editTermIntent);
                    editCourseIntent.putExtra("id", courseId);
                    editCourseIntent.putExtra("title", title);
                    editCourseIntent.putExtra("start", startDates.get(i));
                    editCourseIntent.putExtra("end", endDates.get(i));
                    editCourseIntent.putExtra("startAlert", startAlerts.get(i));
                    editCourseIntent.putExtra("endAlert", endAlerts.get(i));
                    editCourseIntent.putExtra("status", statusList.get(i));
                    editCourseIntent.putExtra("notes", notesList.get(i));
                    editCourseIntent.putExtra("termId", termList.get(i));
                    startActivity(editCourseIntent);
                }
                else{
                    toastMessage("No ID associated with that name");
                }
            }
        });
    }
    private void toastMessage(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
